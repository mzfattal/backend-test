const mongo = require('mongodb').MongoClient
const mongodb = require('mongodb');
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const app = express();

const url = 'mongodb://localhost:27017/mydb'

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongo.connect(url, { useNewUrlParser: true } , (err, client) => {
  if (err) {
    console.error(err);
    return
  }
  console.log("SUCCESS: Connected on http://localhost:3000/");

  const db = client.db('mydb');
  const statsCollection = db.collection('stats');

  const statSchema = Schema ({
    latest: {
      type: Boolean,
      required: false,
      default: false
    },
    schema_version: {
      type: Number,
      required: true,
      min: 1,
      default: false
    },
    asset_type: {
      type: String,
      required: true,
      lowercase: true
    },
    asset_id: {
      type: String,
      required: true,
      minlength: 3
    },
    stat: {
      type: String,
      required: true,
      lowercase: true
    },
    value: {
      type: Number,
      required: true
    },
    created_at: {
      type: Date,
      default: Date.now
    },
    updated_at: {
      type: Date,
      default: Date.now
    },
  });

// saving the schema
const Stat = mongoose.model("Stat", statSchema);

// Get aggregate collection
app.get('/stats/aggregate', async (req, res) => {
  const statAgg = await statsCollection.aggregate([{
    // STEP 1 - keep only `subscribers`, and `posts_30d` regular stats
    $match: {
        $and: [
            {latest: {$ne: true}},
            {stat: { $in: ['subscribers', 'posts_30d'] }} ,
            {asset_type: 'youtube_channel'},
        ],
    }},

    // STEP 2 - changing key of created_at to date
    {
        $project: {
            _id: true,
            schema_version: true,
            asset_type: true,
            asset_id: true,
            stat: true,
            value: true,
            date: '$created_at',
            updated_at: true,
            __v: true,
        }
    },

    // STEP 3 - sort them from oldest to newest
    {$sort: { created_at: 1 }},

    // STEP 4 - group them by channel
    {$group: {
        _id: "$asset_id",
        stats: {$push: '$$ROOT'},
    }},

    // STEP 5 - throw away all channels with less than 10 stats
    {$match: {
        'stats.10': {$exists: true},
    }},

    // STEP 6 - reduce the data set
    {$limit: 300},

    // STEP 7 - we split them based on stat type
    {$project: {
        _id: true,
        stats_subs: {$filter: {
          input: "$stats",
          as: "currentItem",
          cond: {$eq: ['$$currentItem.stat', 'subscribers']},
        }},
        stats_p30d: {$filter: {
          input: "$stats",
          as: "currentItem",
          cond: {$eq: ['$$currentItem.stat', 'posts_30d']},
        }},
    }},

    //STEP 8 - slice off the first 10 of each
    {$project: {
        id: true,
        subscribers: {$slice: ['$stats_subs', 10]},
        posts_30d: {$slice: ['$stats_p30d', 10]},
    }},

    //STEP 9 - projecting only value and date
     { $project: {
        subscribers: {
            value: true,
            date: true,
        },
        posts_30d: {
            value: true,
            date: true,
        }
      }
    },
], { "allowDiskUse" : true }).toArray();
  res.send(statAgg);
});


// GET all records
app.get('/stats', async (req, res) => {
  const stats = await statsCollection.find({}).toArray();
  res.send(stats);
});

// GET record by id
app.get('/stats/:id', async (req, res) => {
  const statId = await statsCollection.findOne({ _id: mongodb.ObjectID(req.params.id)});
  res.send(statId);
});

// UPDATE record
app.patch('/stats/:id', async (req, res) => {
 const statUp = await statsCollection.findOneAndUpdate(
      { _id : mongodb.ObjectID(req.params.id)},
      { $set: req.body });

 res.send(statUp);
});

// CREATE record
app.post('/stats', async (req, res) => {

  const theStat = new Stat ({
    latest: req.body.latest,
    schema_version: req.body.schema_version,
    asset_type: req.body.asset_type,
    asset_id: req.body.asset_id,
    stat: req.body.stat,
    value: req.body.value,
  });

  const statCr = await statsCollection.insertOne(theStat, function (error, response) {
    if(error) {
        console.log('Error occurred while inserting');
    } else {
      res.send(response.ops[0]);
    }
  });

});

// DELETE a record
app.delete('/stats/:id', async (req, res) => {
  const statDel = await statsCollection.findOneAndDelete({ _id: mongodb.ObjectID(req.params.id)});
  res.send(statDel);
});

});

const PORT = 3000;
app.listen(PORT, () => console.log(`listening on port ${PORT}`));
